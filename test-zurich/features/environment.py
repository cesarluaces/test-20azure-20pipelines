from behave.model import Scenario
import library.actions
from behave.log_capture import capture


def before_all(context):
    userdata = context.config.userdata
    continue_after_failed = userdata.getbool("runner.continue_after_failed_step", True)
    Scenario.continue_after_failed_step = continue_after_failed
    context.environment = library.actions.load_environment()
    context.config.setup_logging()


def before_scenario(context, scenario):
    library.actions.load_default_request(context)


def after_scenario(context, scenario):
    try:
        print("-----------------------------------------------------------------")
        print("url: " + str(context.url))
        print("header: " + str(context.header))
        print("request: " + str(context.request))
        print("response: " + str(context.response))
        print("response http status: " + str(context.response.status_code))
        print("response text: " + str(context.response.text))
        print("Time elapsed:" + str(context.response.elapsed.microseconds))
        print("-----------------------------------------------------------------")
    except:
        print("-----------------------------------------------------------------")
        print("Value not found")
        print("-----------------------------------------------------------------")
# @capture
# def after_scenario(context):
#     print("PRNT")
