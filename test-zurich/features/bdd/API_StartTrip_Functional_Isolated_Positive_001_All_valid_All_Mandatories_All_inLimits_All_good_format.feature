@wip
Feature: API_StartTrip_Functional_Isolated_Positive_001_All_valid_All_Mandatories_All_inLimits_All_good_format
Scenario Outline: API_StartTrip_Functional_Isolated_Positive_001_All_valid_All_Mandatories_All_inLimits_All_good_format

	Given we are authenticated
	And we pass <masterPolicyId> as a valid masterPolicyId
	And we pass <contractNumber> as a valid unique contractNumber
	And we pass <id> as a valid id
	And we pass <citizenshipId> as a valid citizenshipId
	And we pass <countryId> as a valid countryId
	And we pass <firstName> as a valid firstName
	And we pass <lastName> as a valid lastName
	And we pass <addressLine1> as a valid addressLine1
	And we pass <addressLine2> as a valid addressLine2
	And we pass <postalCode> as a valid postalCode
	And we pass <birthDate> as a valid birthDate
	And we pass <email> as a valid email
	And we pass <drivingLicenseExpeditionDate> as a valid drivingLicenseExpeditionDate
	And we pass <startDate> as a valid startDate
	And we pass <timeUnitdID> as a valid timeUnitdID
	And we pass <mobilityFormId> as a valid mobilityFormId
	And we pass <vehicleTypeID> as a valid vehicleTypeID
	And we pass <fuelTypeID> as a valid fuelTypeID
	And we pass <horsePower> as a valid horsePower
	And we pass <coverages> as a valid coverages

	When we call to Start Trip Service
	Then error response is false
	And the returned policyId is valid
	And the returned url is valid
	And time elapsed is acceptable


	Examples:
		|masterPolicyId   |contractNumber       |	id	    |	citizenshipId		|	countryId		|	firstName	|	lastName		|	addressLine1		    |	addressLine2                    |	postalCode		|	birthDate		    |	email 					|	drivingLicenseExpeditionDate		|	startDate				    |	timeUnitdID		    |	mobilityFormId		   |	vehicleTypeID		|	fuelTypeID		|	horsePower	|	coverages		                        |
		|1031480100003588  |CtNo-000000103      |	56693   |	    5				|	    US			|	Xavi		|	Fernandez		|	Camp Nou ping	        |   Ciutat Esportiva Joan Gamper    |	08940			|	1985/05/20			|	info1@soporte.com		|	2014/08/13							|	2019/05/14 10:00		    |	MINUTES			    |	CAR_SHARING	           |	TEST				|	ELECTRIC		|	89	        |	PERSONAL_BELONGINGS, PERSONAL_LIABILITY	|
		|1031480100003588  |CtNo-000000104      |	56694   |	   	1			    |		ES	        |	Karla		|	Martínez		|	Avenida Diagonal	    |	N 345 	                        |	08923			|	1983/06/21			|	info1@soporte.com		|	2007/09/22							|	2019/12/01 08:30	        |	HOURS			    |	CAR_SHARING	           |	TEST				|	ELECTRIC		|	89	        |	PERSONAL_BELONGINGS	|
		|1031480100003588  |CtNo-000000105      |	56695   |	   	2			    |		DK	        |	Carlos		|	Muñoz		    |	Gavá	                |	N 346	                        |	08635			|	1983/07/22			|	info1@soporte.com		|	2007/09/19							|	2019/12/29 09:30		    |	DAILY			    |	CAR_SHARING	           |	TEST				|	ELECTRIC		|	89	        |	PERSONAL_BELONGINGS	|
	    |1031480100003588  |CtNo-000000106      |	56696   |	   	6			    |		DE	        |	Francisca	|	Lopez		    |	San Andres de la Barca	|	N 347                          |	08789			|	1983/04/23			|	info1@soporte.com		|	2007/09/24							|	2019/12/15 10:30		    |	ANNUALLY	        |   E_SCOOTER_SHARING      |	TEST				|	ELECTRIC		|	89	        |	PERSONAL_BELONGINGS, LEGAL_EXPENSES|
	    |1031480100003588  |CtNo-000000107      |	56697   |	   	4			    |		GB	        |	Karla		|	Martinez		|	Avenida Diagonal	    |	N 348	                        |	08560			|	1983/09/24			|	info1@soporte.com		|	2006/09/25							|	2019/12/18 07:30	        |	MINUTES			    |	CAR_SHARING	           |	TEST				|	ELECTRIC		|	89	        |	PERSONAL_BELONGINGS	|
		|1031480100003588  |CtNo-000000107      |	56697   |	   	4			    |		GB	        |	Karla		|	Martinez		|	Avenida Diagonal	    |	N 348	                        |	08560			|	1983/09/24			|	info1@soporte.com		|	2006/09/25							|	2019/12/18 07:30	        |	MINUTES			    |	CAR_SHARING	           |	TEST				|	ELECTRIC		|	89	        |   EXCESS_REDUC_THEFT |