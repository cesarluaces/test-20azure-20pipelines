
import json
import logging
from venv import logger
import library.actions
import library.security_control
import os
import datetime


from behave import given, when, then  # pylint: disable=no-name-in-module

time_elapsed_acceptable = 10000000
time_FIELDS = [
    'year', 'month', 'day',
    'hour', 'minute', 'second',
    'microsecond',
]
success_list = [200]
error_list = [400, 401,500]


""""

Generic Functions

"""

def call_service(context,service):

    decoded = ""
    response = library.actions.take_JSON_test_config(context, service +".json")

    if (int(response.status_code) not in error_list):
        try:
            decoded = json.loads(response.text)
            with open('.\\output_' + service + '.json', 'w') as file:
                 json.dump(response.json(), file)
        except:
            logger.exception("ERROR -> result JSON not received. Possible API Web non available")
            raise Exception
   # try:
         #  assert decoded["href"] is not None
         #  assert "collection" in decoded["rel"]
    #except AssertionError as err:
          # logger.exception("ERROR -> Access to " + service +"  failed")
         #  raise err

    context.response = response
    context.response_decoded = decoded
    print(decoded)


def compare_list_with_response_dictionary(context,listofvalues):
    arrayJSON = []
    arrayParameters = [x.strip() for x in listofvalues.split(',')]
    for element in context.response_decoded["value"]:
        arrayJSON.append(element["name"])
    assert (all(elem in arrayJSON for elem in arrayParameters))

"""
    AUTENTICATION SECURITY CALL
"""

@given('we are authenticated')
def step_impl(context):
    context.request_params=""

    response = library.actions.take_JSON_test_config(context,"TakeToken.json")
    decoded = json.loads(response.text)
    token = decoded["access_token"]
    with open('.\\output_TOKEN.json', 'w') as file:
        json.dump(response.json(), file)
    try:
        assert token is not None
    except AssertionError as err:
        logger.exception("ERROR -> Obtain security TOKEN failed")
        raise err
    library.security_control.set_token(token)


@given(u'we are not authenticated')
def step_impl(context):
    context.execute_steps("given we are authenticated")
    library.security_control.set_token("")

"""
    FUNCTIONAL SECTION
"""

@given('POST obtain security token')
def step_impl(context):
    context.execute_steps("given we are authenticated")

@when('we call to Get Policy Statues List')
def step_impl(context):
    call_service(context,"EnumPolicyStatus")

@then('error response is false')
def step_impl(context):
    assert context.response.status_code in success_list

@then('error response is true')
def step_impl(context):
    status_code= int(context.response.status_code)

@then('error list contains the field {fieldName} informing that have incorrect data')
def step_impl(context,fieldName):

   #assert fieldName in  context.response.text
    pass


@then('the return a Policy Statues List "{listofvalues}"')
def step_impl(context, listofvalues):
    compare_list_with_response_dictionary(context, listofvalues)

@then('the returned amount of policy Status is "{numocurrs}"')
def step_impl(context, numocurrs):
         assert len(context.response_decoded["value"]) == int(numocurrs)

@then('time elapsed is acceptable')
def step_impl(context):

     try:
         assert context.response.elapsed.microseconds <= time_elapsed_acceptable
     except AssertionError as err:
         logger.exception("ERROR -> Time elased isn't aceptable ")
         raise err


@when('we call to Get Mobility Forms List Service')
def step_impl(context):
    call_service(context,"EnumMobilityForms")

@then('the return a Mobility Form List "{listofvalues}"')
def step_impl(context, listofvalues):
     compare_list_with_response_dictionary(context,listofvalues)

@then('the returned amount of Mobility Form is "{numocurrs}"')
def step_impl(context, numocurrs):
         assert len(context.response_decoded["value"]) == int(numocurrs)


@when('we call to Get Modalities List Service')
def step_impl(context):
    call_service(context, "EnumModalities")


@then('the return a Modalities List "{listofvalues}"')
def step_impl(context, listofvalues):
    compare_list_with_response_dictionary(context, listofvalues)

@then('the returned amount of Modalities is "{numocurrs}"')
def step_impl(context, numocurrs):
    assert len(context.response_decoded["value"]) == int(numocurrs)


@when('we call to Get Fuel Type List Service')
def step_impl(context):
    call_service(context, "EnumFuelType")

@then('the return a Fuel Type List "{listofvalues}"')
def step_impl(context, listofvalues):
    compare_list_with_response_dictionary(context, listofvalues)


@then('the returned amount of Fuel Type is "{numocurrs}"')
def step_impl(context, numocurrs):
    assert len(context.response_decoded["value"]) == int(numocurrs)


@when('we call to Get Time Units List Service')
def step_impl(context):
    call_service(context, "EnumTimeUnits")


@then('the return a Time Units List "{listofvalues}"')
def step_impl(context, listofvalues):
    compare_list_with_response_dictionary(context,listofvalues)


@then('the returned amount of Time Units is "{numocurrs}"')
def step_impl(context, numocurrs):
    assert len(context.response_decoded["value"]) == int(numocurrs)

@when('we call to Get Countries List Service')
def step_impl(context):
    call_service(context, "EnumCountries")

@then('the return a countries List "{listofvalues}"')
def step_impl(context, listofvalues):
    compare_list_with_response_dictionary(context,listofvalues)

@then('the returned amount of countries is "{numocurrs}"')
def step_impl(context, numocurrs):
    assert len(context.response_decoded["value"]) == int(numocurrs)

@when('we call to Get Citizenships List Service')
def step_impl(context):
    call_service(context,"EnumCitizenship")



@then('the return a citizenships List "{listofvalues}"')
def step_impl(context, listofvalues):
    compare_list_with_response_dictionary(context,listofvalues)


@then('the returned amount of citizenships is "{numocurrs}"')
def step_impl(context, numocurrs):
    assert len(context.response_decoded["value"]) == int(numocurrs)


@when('we call to Get Vehicle Types List')
def step_impl(context):
    call_service(context,"EnumVehicleType")


@then('the return a Vehicle Types List "{listofvalues}"')
def step_impl(context, listofvalues):
    compare_list_with_response_dictionary(context,listofvalues)


@then('the returned amount of Vehicle Types is "{numocurrs}"')
def step_impl(context, numocurrs):
    assert len(context.response_decoded["value"]) == int(numocurrs)



@given('we pass {masterPolicyId} as a valid masterPolicyId')
def step_impl(context, masterPolicyId):
    context.request_params = [{'name': 'masterPolicyId', 'value': masterPolicyId}]


@given('we pass {JSON_Request_File} a JSON request file')
def step_impl(context, JSON_Request_File):
    pass


@given('we pass {masterPolicyId} as a not valid masterPolicyId')
def step_impl(context, masterPolicyId):
    context.request_params = [{'name': 'masterPolicyId', 'value': masterPolicyId}]


@given('we pass {contractNumber} as a valid unique contractNumber')
def step_impl(context, contractNumber):
    new_contract_number=""
    d = datetime.datetime.now()
    for attr in time_FIELDS:
       new_contract_number=new_contract_number+str(getattr(d, attr))
    context.request["contractNumber"] = "API-Test-TCOE" + new_contract_number


@given('we pass {id} as a valid id')
def step_impl(context,id):
    context.request["customer"]["id"] = id


@given('we pass {citizenshipId} as a valid citizenshipId')
def step_impl(context,citizenshipId):
    context.request["customer"]["citizenshipId"] = citizenshipId

@given(u'we pass {citizenshipId} as a not valid citizenshipId')
def step_impl(context, citizenshipId):
    context.request["customer"]["citizenshipId"] = citizenshipId

@given('we pass {countryId} as a valid countryId')
def step_impl(context,countryId):
    context.request["customer"]["countryId"]=countryId


@given(u'we pass {countryId} as a not valid countryId')
def step_impl(context,countryId):
    context.request["customer"]["countryId"]=countryId

@given('we pass {firstName} as a valid firstName')
def step_impl(context,firstName):
    context.request["customer"]["firstName"] = firstName

@given('we pass {lastName} as a valid lastName')
def step_impl(context,lastName):
    context.request["customer"]["lastName"] = lastName

@given('we pass {addressLine1} as a valid addressLine1')
def step_impl(context,addressLine1):
    context.request["customer"]["addressLine1"] = addressLine1

@given('we pass {addressLine2} as a valid addressLine2')
def step_impl(context,addressLine2):
    context.request["customer"]["addressLine2"] = addressLine2

@given('we pass {postalCode} as a valid postalCode')
def step_impl(context,postalCode):
    context.request["customer"]["postalCode"] = postalCode

@given('we pass {birthDate} as a valid birthDate')
def step_impl(context, birthDate):
    context.request["customer"]["birthDate"] = birthDate

@given(u'we pass {birthDate} as a not valid birthDate')
def step_impl(context,birthDate):
    context.request["customer"]["birthDate"] = birthDate

@given('we pass {email} as a valid email')
def step_impl(context,email):
    context.request["customer"]["email"] = email

@given('we pass {drivingLicenseExpeditionDate} as a valid drivingLicenseExpeditionDate')
def step_impl(context,drivingLicenseExpeditionDate):
    context.request["customer"]["drivingLicenseExpeditionDate"] = drivingLicenseExpeditionDate

@given(u'we pass {drivingLicenseExpeditionDate} as a not valid drivingLicenseExpeditionDate')
def step_impl(context,drivingLicenseExpeditionDate):
    context.request["customer"]["drivingLicenseExpeditionDate"] = drivingLicenseExpeditionDate

@given('we pass {startDate} as a valid startDate')
def step_impl(context,startDate):
    context.request["trip"]["startDate"] = startDate

@given(u'we pass {startDate} as a not valid startDate')
def step_impl(context,startDate):
    context.request["customer"]["startDate"] = startDate

@given('we pass {timeUnitdID} as a valid timeUnitdID')
def step_impl(context,timeUnitdID):
    context.request["trip"]["timeUnitId"] = timeUnitdID

@given('we pass {mobilityFormId} as a valid mobilityFormId')
def step_impl(context,mobilityFormId):
    context.request["trip"]["mobilityFormId"] = mobilityFormId

@given('we pass {vehicleTypeID} as a valid vehicleTypeID')
def step_impl(context,vehicleTypeID):
    context.request["trip"]["vehicleTypeId"] = vehicleTypeID

@given('we pass {fuelTypeID} as a valid fuelTypeID')
def step_impl(context,fuelTypeID):
    context.request["trip"]["fuelTypeId"] = fuelTypeID

@given('we pass {horsePower} as a valid horsePower')
def step_impl(context, horsePower):
    context.request["trip"]["horsePower"] = horsePower

@given('we pass {coverages} as a valid coverages')
def step_impl(context, coverages):
    context.request["coverages"] = [coverages]

@then('the returned policyId is valid')
def step_impl(context):
    assert context.response_decoded["policyId"] is not None

@then('the returned url is valid')
def step_impl(context ):
   assert context.response_decoded["href"] is not None

@when('we call to Start Trip Service')
def step_impl(context):
    call_service(context,"StartTrip")

@given(u'we pass all valid variables')
def step_impl(context):
    pass

@given(u'we pass {countryId} as a not valid country identifier')
def step_impl(context,countryId):
    context.request["customer"]["countryId"] = countryId