@error
Feature: API_StartTrip_Functional_Isolated_Positive_002_Load_From_JSON.feature
Scenario Outline: API_StartTrip_Functional_Isolated_Positive_002_Load_From_JSON.feature

	Given we are authenticated
	And we pass <JSON_Request_File> a JSON request file
	When we call to Start Trip Service
	Then error response is false
	And the returned policyId is valid
	And the returned url is valid
	And time elapsed is acceptable


	Examples:
		|JSON_Request_File   |
		|API_Start_Trip_Functional_Isolated_From_JSON_001.json	 |
