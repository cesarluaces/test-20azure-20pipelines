@wip
Feature:API_StartTrip_Functional_Isolated_Negative_0021_WrongFormat_fuelTypeID_All_rest_valid.feature
Scenario Outline: API_StartTrip_Functional_Isolated_Negative_0021_WrongFormat_fuelTypeID_All_rest_valid.feature

	Given we are authenticated
	And we pass <masterPolicyId> as a valid masterPolicyId
	And we pass <contractNumber> as a valid unique contractNumber
	And we pass <id> as a valid id
	And we pass <citizenshipId> as a valid citizenshipId
	And we pass <countryId> as a not valid countryId
	And we pass <firstName> as a valid firstName
	And we pass <lastName> as a valid lastName
	And we pass <addressLine1> as a valid addressLine1
	And we pass <addressLine2> as a valid addressLine2
	And we pass <postalCode> as a valid postalCode
	And we pass <birthDate> as a valid birthDate
	And we pass <email> as a valid email
	And we pass <drivingLicenseExpeditionDate> as a valid drivingLicenseExpeditionDate
	And we pass <startDate> as a valid startDate
	And we pass <timeUnitdID> as a valid timeUnitdID
	And we pass <mobilityFormId> as a valid mobilityFormId
	And we pass <vehicleTypeID> as a valid vehicleTypeID
	And we pass <fuelTypeID> as a valid fuelTypeID
	And we pass <horsePower> as a valid horsePower
	And we pass <coverages> as a valid coverages

	When we call to Start Trip Service
	Then error response is True



	Examples:
		|masterPolicyId   |contractNumber|	id	    |	citizenshipId		|	countryId		|	firstName	|	lastName		|	addressLine1		|	addressLine2|	postalCode		|	birthDate		    |	email 					|	drivingLicenseExpeditionDate		|	startDate				|	timeUnitdID		|	mobilityFormId		|	vehicleTypeID		|	fuelTypeID		|	horsePower		|	coverages		    |
		|1031480100003180|CTN-201912191550005 |	345678  |	    9999999				|	US			    |	Karla		|	nunez		    |	Avenida Diagonal	|	67	        |	08940			|	1983/02/07			|	info1@soporte.com		|	2014/08/13							|	2019/05/14		        |	MINUTES			|	CAR_SHARING	|	TEST				|	ELECTRIC		|	89			    |	PERSONAL_BELONGINGS	|
