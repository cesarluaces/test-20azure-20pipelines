Feature: DIALOGUE Behave examples

  Scenario: POST Access Token

    Given POST obtain security token

  Scenario Outline: GET Time Unit, Modality, Fuel & Mobility form

     When GET access to "<url_base><url_comp>"
     Then Obtain ID of each "<value>"
     Then Add values to the attribute "<title>" in JSON file "<json_file>"
     Then Check response with value "<response>" in field "<assertion>"

    Examples:
    | url_base                                    |  url_comp     |  title         |value       |json_file                                                       |assertion |response        |
    | https://sit-mobility-apim.azure-api.net/api | /mobilityForms|  mobilityForm  |Carsharing  |../test-zurich/configs/json_post_folder/JSON_to_policy_quote.json|   rel   |   collection   |
    | https://sit-mobility-apim.azure-api.net/api | /timeUnits    |  timeUnit      |Minutes     |../test-zurich/configs/json_post_folder/JSON_to_policy_quote.json|   rel   |   collection   |
    | https://sit-mobility-apim.azure-api.net/api | /fuelTypes    |  fuel          |Gasoil      |../test-zurich/configs/json_post_folder/JSON_to_policy_quote.json|   rel   |   collection   |
    | https://sit-mobility-apim.azure-api.net/api | /modalities   |  modality      |Silver      |../test-zurich/configs/json_post_folder/JSON_to_policy_quote.json|   rel   |   collection   |

    Scenario Outline: POST New Quote

      When POST access to "<url_base><url_comp>" with "<json_file>" data

    Examples:
    | url_base                                    |  url_comp                        |  json_file                                                      |
    | https://sit-mobility-apim.azure-api.net/api | /policies/1031480100002547/quotes|../test-zurich/configs/json_post_folder/JSON_to_policy_quote.json|

    Scenario Outline: POST Start Trip -> PUT End Trip

      When POST access to "<url_base><url_comp_start>" with "<json_file_start>" data
      Then PUT access to "<url_base><url_comp_end>" with "<json_file_end>" data

    Examples:
    | url_base                                    |  url_comp_start                           |  url_comp_end                           |  json_file_start                                              |                                      json_file_end                            |
    | https://sit-mobility-apim.azure-api.net/api | /policies/1031480100002547/startTrip      | /policies/1031480100002547/endTrip      |../test-zurich/configs/json_post_folder/JSON_to_start_trip.json|../test-zurich/configs/json_post_folder/JSON_to_end_trip.json|
