@final
Feature: API_EnumPolicyStatus_Functional_Isolated_Positive_001_All_valid_All_Mandatories_All_inLimits_All_good_format
Scenario: API_EnumPolicyStatus_Functional_Isolated_Positive_001_All_valid_All_Mandatories_All_inLimits_All_good_format

	Given we are authenticated
	When we call to Get Policy Statues List
	Then error response is false
	And the return a Policy Statues List "Retired,Active,New,Inactive,Waiting,Undone,Ready for Activation"
	And the returned amount of policy Status is "7"
    And time elapsed is acceptable


	

	