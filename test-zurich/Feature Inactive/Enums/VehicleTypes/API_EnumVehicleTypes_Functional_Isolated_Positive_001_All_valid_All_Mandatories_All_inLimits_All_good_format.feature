@final
Feature: API_EnumVehicleTypes_Functional_Isolated_Positive_001_All_valid_All_Mandatories_All_inLimits_All_good_format
Scenario: API_EnumVehicleTypes_Functional_Isolated_Positive_001_All_valid_All_Mandatories_All_inLimits_All_good_format

	Given we are authenticated
	When we call to Get Vehicle Types List
	Then error response is false
	And the return a Vehicle Types List "TEST"
	And the returned amount of Vehicle Types is "1"
    And time elapsed is acceptable


	

	