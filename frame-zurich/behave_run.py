import datetime
import sys
import library.executing
import library.actions
import library.generateYML
from behave import __main__ as behave_executable

print('Started at:', datetime.datetime.now())

if sys.argv[1:]:
    # behave_executable.main(' '.join(sys.argv[1:]))
    behave_executable.main('-f allure_behave.formatter:AllureFormatter -o ./reports ./features')
else:
    # behave_executable.main(make_behave_argv())
    # behave_executable.main(make_behave_argv(conf_properties='android'))
    # behave_executable.main(make_behave_argv(tags=['regression', ['-android_only', '-wip']]))
    # behave_executable.main(make_behave_argv(tags='-no_driver'))
    # behave_executable.main(make_behave_argv(environment='epi'))

    parameters = library.executing.make_behave_argv(tags=['wip'])
    active_tags = library.actions.extract_data_from_excel("config.xlsx", "test_config", "ACTIVE_TAGS")
    behave_executable.main(library.executing.make_behave_argv(tags=[active_tags]))

print('Ended at:', datetime.datetime.now())

# # examples:
# behave_executable.main(None)
# behave_executable.main('--tags @wip')
# behave_executable.main('--tags @android_only')
# behave_executable.main('--junit --tags @wip')
# behave_executable.main('-v --junit --format pretty --tags ~@android_only --tags ~@wip')
# behave_executable.main('-f json -o ./reports/data.json --tags @wip')
# behave_executable.main('-f allure_behave.formatter:AllureFormatter -o ./reports ./features')
