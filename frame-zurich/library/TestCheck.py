
import logging
import json


class TestCheck:
    '''
        This class implements a checker for the value of a parameter.
        This value has a origin, which this class will find and retrieve the value
        associated to it.
        Then, it will be possible to compare this value with the one we get calling
        services.
    '''

    def __init__(self):
        '''
            Class constructor.
        '''
        self.conn_db = None
        self.file_data = None
        self.context = None
        logging.info('-----------------------------------------------------------------------------------------')
        logging.info(' TestCheck instance created.')
        logging.info('-----------------------------------------------------------------------------------------')

    def set_context(self, ctx):
        logging.info('-----------------------------------------------------------------------------------------')
        logging.info(' TestCheck set_context start...')
        if ctx is not None:
            self.context = ctx
            logging.info(' Context loaded successfully.')
            logging.info(' TestCheck set_context ended OK.')
        else:
            logging.error(' Failed loading context: param "ctx" is None.')
            logging.error(' TestCheck set_context ended KO.')
            raise ValueError("Context is null")
        logging.info('-----------------------------------------------------------------------------------------')

    def open_data_pool(self, data_pool):
        '''
            This function opens a file and loads its content into a local variable.
        '''
        try:
            logging.info('-----------------------------------------------------------------------------------------')
            logging.info(' TestCheck instance created.')
            with open(data_pool, 'r') as input_file:
                logging.info(' file opened as read only.')
                content = input_file.read()
                self.file_data = json.loads(content)
                logging.info(' file data saved into a local variable successfully.')
                logging.info(' TestCheck open_data_pool ended OK.')
                logging.info(
                    '-----------------------------------------------------------------------------------------')
        except Exception as e:
            logging.exception(' - EXCEPTION: TestCheck open_data_pool')
            logging.exception(' - '+str(type(e)))
            logging.exception(' - '+str(e.args))
            logging.exception(' TestCheck open_data_pool ended KO.')
            raise

    def get_origin(self, parameter):
        '''
            This function searches on the file if the :param parameter:'s value is on the file or it's on a database
            and :return:s a value that indicates one or another. It also :return:s -1 if the :param parameter: wasn't
            found.
        '''
        logging.info(' TestCheck get_origin start...')
        try:
            logging.info(' searching for key ' + str(parameter) + ' inside the dictionary')
            for k, v in self.file_data.items():
                if parameter == k:
                    logging.info(' ' + str(parameter) + ' found in dictionary.')
                    if isinstance(v, str):
                    # if str(v).find("DATABASE") > 0:
                        logging.info(' It\'s origin is a FILE')
                        logging.info(
                            '-----------------------------------------------------------------------------------------')
                        return "FILE"
                    elif isinstance(v ,list):
                        logging.info(' It\'s origin is a FILE')
                        logging.info(
                            '-----------------------------------------------------------------------------------------')
                        return "FILE"
                    elif isinstance(v, dict):
                        if "database" in v.keys():
                            logging.info(' It\'s origin is a DATABASE')
                            logging.info(
                                '-----------------------------------------------------------------------------------------')
                            return "DATABASE"
                        else:
                            logging.info(' It\'s origin is a FILE')
                            logging.info(
                                '-----------------------------------------------------------------------------------------')
                            return "FILE"
                    else:
                        logging.info(' It\'s origin is a DATABASE')
                        logging.info(
                            '-----------------------------------------------------------------------------------------')
                        return "DATABASE"
            logging.error(' It\'s origin is unknown')
            logging.error(
                '-----------------------------------------------------------------------------------------')
            return -1
        except Exception as e:
            logging.exception(' EXCEPTION: TestCheck get_origin')
            logging.exception(' - ' + str(type(e)))
            logging.exception(' - ' + str(e.args))
            logging.exception(' TestCheck get_origin ended KO.')
            raise

    def check(self, key, given_value):
        '''
            This function compares :param given_value: with the value retrieved
            from the :param key:'s origin and :return:s the result of the comparison.
        '''
        logging.info('-----------------------------------------------------------------------------------------')
        logging.info(' TestCheck check start...')
        logging.info('-----------------------------------------------------------------------------------------')
        try:
            self.open_data_pool("io/values.json")
            logging.info(' data pool oppened successfully.')
            origin = self.get_origin(key)
            logging.info(' origin found: ' + str(origin))
            value = None
            if origin == "FILE":
                value = self.get_value(key)
                logging.info(' value found in file: ' + str(value))
            elif origin == "DATABASE":
                self.get_database(key)
                # result = self.execute_query(key)
                # value = result[key]
                value = self.execute_query(key)
                logging.info(' value found in database: ' + str(value))
            else:
                raise KeyError(" Key not found on values.json file.")
            logging.info(str(given_value) + " vs. " + str(value))
            assert value == given_value, " ASSERT ERROR: VALUES ARE NOT THE SAME."
            logging.info('-----------------------------------------------------------------------------------------')
            logging.info(' ASSERT OK. TestCheck check ended OK.')
            logging.info('-----------------------------------------------------------------------------------------')
        except Exception as e:
            logging.exception(' EXCEPTION: TestCheck check')
            logging.exception(' - ' + str(type(e)))
            logging.exception(' - ' + str(e.args))
            logging.exception(' TestCheck check ended KO.')
            raise

    def get_value(self, key):
        '''
            This function gets the value associated to :param key: inside a opened file and
            :return:s it.
        '''
        try:
            logging.info(' TestCheck get_value(key) start...')
            value = self.search2(key, self.file_data)
            logging.info(' TestCheck get_value(key) ended OK. + ' + str(value))
            return value
        except Exception as e:
            logging.exception(' EXCEPTION: TestCheck get_value')
            logging.exception(' - ' + str(type(e)))
            logging.exception(' - ' + str(e.args))
            logging.exception(' TestCheck get_value ended KO.')
            raise

    def search2(self, key, dictio):
        '''
            This is the function that can be called recursively.
        '''
        try:
            # logging.info(' searching for key: ' + key)
            for k, v in dictio.items():
                # logging.info(' current k: ' + str(k))
                if k == key:
                    logging.info(' found: ' + str(v))
                    return v
                elif isinstance(v, dict):
                    # logging.info(' dictionary associated to k.')
                    result = self.search2(key, v)
                    if result is not None:
                        logging.info(' found: ' + str(v))
                        return result
                elif isinstance(v, list):
                    # logging.info(' list associated to k.')
                    for d in v:
                        if isinstance (d, dict):
                            # logging.info(' dictionary associated to d element of k\'s value.')
                            result = self.search2(key, d)
                            if result is not None:
                                logging.info(' found: ' + str(v))
                                return result
        except Exception as e:
            logging.exception(' EXCEPTION: TestCheck search2')
            logging.exception(' - ' + str(type(e)))
            logging.exception(' - ' + str(e.args))
            logging.exception(' TestCheck search2 ended KO.')
            raise

    def execute_query(self, db):
        '''
            This function uses :param connection: to execute a :param query: and retrieve the value
            associated to a key. This key will be inside the :param query:. The value is :return:ed.
        '''
        logging.info(' TestCheck execute_query(query) start...')
        try:
            connection_data = self.get_value(db)
            query = connection_data["query"]
            cursor = self.conn_db.cursor().execute(query)
            # keyes = [keyes[0] for keyes in cursor.description]
            # results = []
            # for row in cursor.fetchall():
            #     results.append(dict(zip(keyes, row)))
            # return results[0]
            result = str(cursor.fetchone())
            return result[result.find("('")+2:result.find(" ',")-3]
        except Exception as e:
            logging.exception(' - EXCEPTION: TestCheck execute_query')
            logging.exception(' - ' + str(type(e)))
            logging.exception(' - ' + str(e.args))
            logging.exception(' TestCheck execute_query ended KO.')
            raise
