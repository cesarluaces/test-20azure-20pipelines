import json
import os
import logging
import uuid


class TestContext:
    '''
        This class contains a handler of what we define as Context.
        Context class is a json format dictionary that contains pairs of key : value with information
        used on the API, such as username, password, auth_token, etc.
        This class has 4 methods:
            *create: this method creates a context json file whose name is an uuid from an original conf_file.

            *search(key): this method returns the value associated within a given key

            *persist(key, value): this method adds a pair of key:value onto the context file

    '''
    def __init__(self):
        '''
            This is the TestContext class constructor.
            It doesnt initialize any variable
        '''
        self.uuid = None
        self.context = {}
        logging.info('-----------------------------------------------------------------------------------------')
        logging.info(' Context instance created.')
        logging.info('-----------------------------------------------------------------------------------------')

    def create(self, conf_file):
        '''
            This function creates the context file using the content of the :param conf_file: attribute.
            File opening exceptions will be handled separately.
            This method will :return: 0 on success and -1 if any exception happens
            Exceptions with the file opening and file writing are handled, and the exception type and params are printed
            on console for debugging.
        '''
        logging.info('-----------------------------------------------------------------------------------------')
        logging.info(' Context create() start...')
        try:
            with open(conf_file, "r") as input_file:
                logging.info(' Context create() ' + conf_file + ' opened successfully.')
                content = input_file.read()
                self.context = json.loads(content)
                logging.info(' File data was parsed on a class variable successfully.')
                self.uuid = uuid.uuid1()
                self.writedown("io/contexts/" + str(self.uuid) + ".json")
                logging.info(' Context create() ended OK.')
                logging.info(
                    '-----------------------------------------------------------------------------------------')
        except Exception as e:
            logging.exception(' EXCEPTION context.create().')
            logging.exception(' - ' + str(type(e)))
            logging.exception(' - ' + str(e.args))
            logging.exception(' Context create() ended KO.')
            raise

    def search(self, key):
        '''
            This function searches recursively for the :param key: onto the context dictionary
            or sub-dictionaries.
            It :return: the value associated if it exists.
        '''
        try:
            logging.info(' Context search(key) start...')
            value = self.search2(key, self.context)
            logging.info(' Context search(key) ended OK. + ' + str(value))
            return value
        except Exception as e:
            logging.exception(' EXCEPTION context.search().')
            logging.exception(' - ' + str(type(e)))
            logging.exception(' - ' + str(e.args))
            logging.exception(' Context search() ended KO.')
            raise

    def search2(self, key, dictio):
        '''
            This is the function that can be called recursively.
        '''
        try:
            # logging.info(' searching for key: ' + key)
            for k, v in dictio.items():
                # logging.info(' current k: ' + str(k))
                if k == key:
                    logging.info(' found: ' + str(v))
                    return v
                elif isinstance(v, dict):
                    # logging.info(' dictionary associated to k.')
                    result = self.search2(key, v)
                    if result is not None:
                        logging.info(' found: ' + str(v))
                        return result
                elif isinstance(v, list):
                    # logging.info(' list associated to k.')
                    for d in v:
                        if isinstance(d, dict):
                            # logging.info(' dictionary associated to d element of k\'s value.')
                            result = self.search2(key, d)
                            if result is not None:
                                logging.info(' found: ' + str(v))
                                return result
        except Exception as e:
            logging.exception(' EXCEPTION context.search2().')
            logging.exception(' - ' + str(type(e)))
            logging.exception(' - ' + str(e.args))
            logging.exception(' Context search2() ended KO.')
            raise

    def persists_kv(self, key, value):
        '''
            This method adds the pair :param key:, :param value: to the local variable context,
            then writes it down into the file.
        '''
        try:
            logging.info('-----------------------------------------------------------------------------------------')
            logging.info(' Context persists(key, value) start...')
            # if key in self.context:
            #     indexes = range(1,100,1)
            #     for n in indexes:
            #         version = str(key) + "_" + str(n)
            #         if version not in self.context:
            #             self.context[version] = value
            #             break
            # else:
            self.context[key] = value
            self.writedown("io/contexts/" + str(self.uuid) + ".json")
            logging.info(' Dictionary updated successfully.')
            logging.info('-----------------------------------------------------------------------------------------')
        except Exception as e:
            logging.exception(' EXCEPTION context.persists_kv().')
            logging.exception(' - ' + str(type(e)))
            logging.exception(' - ' + str(e.args))
            logging.exception(' Context persists_kv() ended KO.')
            raise

    def persists(self, dictio):
        '''
            This method adds the pair :param dictio: to the local variable context, then
            writes it down into the file.
        '''
        try:
            logging.info('-----------------------------------------------------------------------------------------')
            logging.info(' Context persists(dict) start...')
            self.context.update(dictio)
            self.writedown("io/contexts/" + str(self.uuid) + ".json")
            logging.info(' Dictionary updated successfully.')
            logging.info('-----------------------------------------------------------------------------------------')
        except Exception as e:
            logging.exception(' EXCEPTION context.persists().')
            logging.exception(' - ' + str(type(e)))
            logging.exception(' - ' + str(e.args))
            logging.exception(' Context persists() ended KO.')
            raise

    def writedown(self, file):
        '''
            This method writes down the local context dictionary into the specified file.
            :return: is for debugging.
        '''
        logging.info(' Context writedown() start...')
        try:
            with open(file, "w") as output_file:
                output_file.write(json.dumps(self.context, sort_keys=False, indent=4, separators=(',', ' : ')))
                logging.info(' Context file updated successfully.')
                return 0
        except Exception as e:
            logging.exception(' EXCEPTION context.writedown().')
            logging.exception(' - ' + str(type(e)))
            logging.exception(' - ' + str(e.args))
            logging.exception(' Context writedown() ended KO.')
            raise

    def free(self):
        '''
            This function deletes the context file.
        '''
        try:
            os.remove("io/contexts/" + str(self.uuid) + ".json")
        except Exception as e:
            logging.exception(' EXCEPTION context.free().')
            logging.exception(' - ' + str(type(e)))
            logging.exception(' - ' + str(e.args))
            logging.exception(' Context free() ended KO.')
            raise
