import json
import http.client
import logging
import time as timer
import ast


class TestServices:
    '''
        This class is a handler for the methods that the API REST supports.
        They are defined in a JSON file. This handler interpretes this file and
        dynamically creates the requests for this API.
        The meaning of the class vars is:
            - data: contains all the data from the services.json file,
                    which is the description of all the services.
            - context: contains a reference to the context used on all the tests.

            - url, method, header, payload, id: they are data from a specific
                    service. They are informed by the method get_parameters.
    '''

    def __init__(self):
        '''
            This is the constructor for the class Services.
            The only thing it does is opening the JSON file and writing its
            content on the class variable "data".
        '''
        self.data = {}
        self.context = None
        self.url, self.method, self.header, self.payload, self.id = None, None, None, None, None
        logging.info('-----------------------------------------------------------------------------------------')
        logging.info(' Service instance created.')
        try:
            logging.info(' Services __init__ start...')
            with open("io/services.json", "r") as input_file:
                logging.info(' Services.json open successfully.')
                text = input_file.read()
                self.data = json.loads(text)
                logging.info(' Data parsed into a class variable (self.data) successfully.')
                logging.info(' Services __init__ ended OK.')
                logging.info(
                    '-----------------------------------------------------------------------------------------')
        except Exception as e:
            logging.exception(' - EXCEPTION: Services __init__')
            logging.exception(' - ' + str(type(e)))
            logging.exception(' - ' + str(e.args))
            logging.exception(' Services __init__ ended KO.')
            raise

    def set_context(self, ctx):
        '''
            This function integrates an existing context with the created Services object.
        '''
        logging.info('-----------------------------------------------------------------------------------------')
        logging.info(' Services set_context start...')
        if ctx is not None:
            self.context = ctx
            logging.info(' Context loaded successfully.')
            logging.info(' Services set_context ended OK.')
        else:
            logging.error(' Failed loading context: param "ctx" is None.')
            logging.error(' Services set_context ended KO.')
            raise ValueError("Context is null")
        logging.info('-----------------------------------------------------------------------------------------')

    def get_parameters(self, pid):
        '''
            This function gets the needed parameters to create a request for the :param id: method
            from the data read from the JSON file.
            They are refactored with values saved on context
        '''
        logging.info('-----------------------------------------------------------------------------------------')
        logging.info(' Services get_parameters start.')
        logging.info(' Looking for the service: \"' + str(pid) + '\"')
        try:
            if self.data is None:
                logging.error(' ERROR: self.data variable is None: can\'t get ' + str(pid) + ' parameters.')
                logging.error(' Services get_parameters ended KO.')
                raise ValueError("parameter id is null.")
            else:
                methods = self.data["item"]
                logging.info(' self.data["item"] (all methods) loaded successfully, for loop starts:')
                for n in methods:
                    logging.info(' current value: ' + n["name"] + '.')
                    if n["name"] == pid:
                        logging.info(' - '+pid+' was found.')
                        self.id = pid
                        self.url = n["request"]["url"]
                        logging.info(' - self.url was loaded: ' + str(self.url))
                        self.method = n["request"]["method"]
                        logging.info(' - self.method was loaded: ' + str(self.method))
                        if n["request"]["header"] == []:
                            self.header = {}
                        else:
                            self.header = n["request"]["header"][0]
                        logging.info(' - self.header was loaded: ' + str(self.header))
                        if n["request"]["body"] == {}:
                            self.payload = {}
                        else:
                            self.payload = n["request"]["body"]["raw"]
                        logging.info(' - self.payload was loaded: ' + str(self.payload))
                        break
            logging.info('-----------------------------------------------------------------------------------------')
        except Exception as e:
            logging.exception(' - EXCEPTION: Services get_parameters()')
            logging.exception(' - ' + str(type(e)))
            logging.exception(' - ' + str(e.args))
            logging.exception(' Services get_parameters() ended KO.')
            raise

    def prepare_params(self, project=None, username=None, password=None, time=False):
        '''
            This function refactors all parameters needed to do the service's call.
            It also has non mandatory variables used to choose between list members.
        '''
        logging.info('-----------------------------------------------------------------------------------------')
        logging.info(' Services prepare_params start.')
        logging.info('-----------------------------------------------------------------------------------------')
        try:
            self.url = self.refactor(str(self.url), choice=project)
            if " " in self.url:
                self.url = self.url.replace(" ", "%20")
                logging.info(" url whitespaces refactored")
            if time:
                logging.info(' necesity to append actual time to url')
                self.url += str(timer.time()*1000).split(".")[0]
            logging.info('-----------------------------------------------------------------------------------------')
            logging.info(' url refactored: ' + str(self.url))
            logging.info('-----------------------------------------------------------------------------------------')
            self.method = self.refactor(str(self.method))
            logging.info('-----------------------------------------------------------------------------------------')
            logging.info(' method refactored: ' + str(self.method))
            logging.info('-----------------------------------------------------------------------------------------')
            # if self.header != {}:
            #     key = self.header["key"]
            #     value = self.header["value"]
            #     value = self.refactor(str(value), choice=username, choice2=password)
            #     aux={}; aux[key] = value
            #     self.header = aux
            self.header = self.refactor(str(self.header), choice=username, choice2=password)
            self.header = ast.literal_eval(self.header)
            logging.info('-----------------------------------------------------------------------------------------')
            logging.info(' header refactored: ' + str(self.header))
            logging.info('-----------------------------------------------------------------------------------------')
            self.payload = self.refactor(str(self.payload), choice=project)
            logging.info('-----------------------------------------------------------------------------------------')
            logging.info(' payload refactored: ' + str(self.payload))
            logging.info('-----------------------------------------------------------------------------------------')
        except Exception as e:
            logging.exception(' - EXCEPTION: Services prepare_params()')
            logging.exception(' - ' + str(type(e)))
            logging.exception(' - ' + str(e.args))
            logging.exception(' Services prepare_params() ended KO.')
            raise

    def call(self, persist=None, project=None, username=None, password=None, time=False):
        '''
            This function creates the call to the function that its parameters were previously
            gotten from the JSON file. It :return: the response from server.
            Aditionally, if :param persist: is True, it will add the response to the context
            dictionary.
        '''
        logging.info('-----------------------------------------------------------------------------------------')
        logging.info(' Services call start...')
        logging.info('-----------------------------------------------------------------------------------------')
        logging.info(' prepare_params called.')
        try:
            self.prepare_params(project=project, username=username, password=password, time=time)
            splits = []
            splits = self.url.split("/")
            server = splits[2]
            api = '/'.join(map(str, splits[3:]))
            api = "/"+api
            connection = http.client.HTTPConnection(server)
            connection.request(self.method, api, self.payload, self.header)
            logging.info(' request done.')
            response = connection.getresponse()
            response_data = response.read()
            logging.info('-----------------------------------------------------------------------------------------')
            logging.info(' - status: ' + str(response.status))
            logging.info(' - reason: ' + str(response.reason))
            logging.info(' - response.read(): ' + str(response))
            dict_data = json.loads(response_data)
            logging.info(' Response: ' + str(dict_data))
            if persist is True:
                self.context.persists_kv(self.id, dict_data)
                logging.info(' Response persisted into context.')
            logging.info(' Services set_context ended OK.')
            logging.info('-----------------------------------------------------------------------------------------')
            return dict_data
        except Exception as e:
            logging.exception(' - EXCEPTION: call()')
            logging.exception(' - ' + str(type(e)))
            logging.exception(' - ' + str(e.args))
            logging.exception(' Services call() ended KO.')
            raise

    def refactor(self, text, choice=0, choice2=0):
        '''
            This function :return: the :param text: changing the context key with its values, if theres
            any. Else, it returns the same text.
        '''
        logging.info(' Services refactor start...')
        try:
            if self.context is not None:
                if "{{" in text and "}}" in text:
                    key = text[text.find("{{")+2: text.find("}}")]
                    logging.info(' Refactoring key ' + key)
                    value = self.context.search(key)
                    if isinstance(value, list):
                        value = value[choice]
                    if value is not None:
                        return self.refactor(text[:text.find("{{")] + str(value) + text[text.find("}}")+2:], choice=choice)
                    else:
                        logging.error(' refactor: ' + key + ' not found.')
                        return text
                else:
                    logging.info(' no text to refactor')
                    return text
            else:
                logging.info(' no context to refactor')
                return text
        except Exception as e:
            logging.exception(' - EXCEPTION: refactor()')
            logging.exception(' - ' + str(type(e)))
            logging.exception(' - ' + str(e.args))
            logging.exception(' Services refactor() ended KO.')
            raise
