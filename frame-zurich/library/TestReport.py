import logging
import os
from json2html import *
import json
import time as timer


class TestReport:
    '''
        This class stores a report of each step duration and result.
    '''
    # path = "reports/raw/"
    # logrute = 'C:\\Users\\mtp1818\\Documents\\zurich\\zurich\\frame-zurich\\io\\log.txt'

    def __init__(self):
        '''
            Class constructor
        '''
        self.path = "reports/raw/"
        # self.logrute = 'reports/log_' + str(timer.strftime("%d%m%Y", timer.gmtime()) + "-" + timer.strftime("%H%M")) +'.txt'
        # self.logrute = 'C:\\Users\\mtp1818\\Documents\\zurich\\zurich\\frame-zurich\\io\\log.txt'
        self.logrute = 'D:\\MTP\\DEMO\\CorreosPythonBehave\\Results\\log_' + str(timer.strftime("%d%m%Y", timer.gmtime()) + "-" + timer.strftime("%H%M")) +'.txt'
        logging.basicConfig(filename=self.logrute, filemode='a',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S', level=logging.DEBUG)
        logging.info('/////////////////////////////////////////////////////////////////////////////////////////')
        logging.info('/////////////////////////////////////////////////////////////////////////////////////////')
        logging.info('/////////////////////////////////////////////////////////////////////////////////////////')
        logging.info('/////////////////////////////////////////////////////////////////////////////////////////')
        logging.info(' - LOG START - ')
        logging.info('-----------------------------------------------------------------------------------------')

    def generate_report(self):
        '''
            This function reads all raw jsons from a fixed path and generates graphs and html reports with
            that data.
        '''
        logging.info('-----------------------------------------------------------------------------------------')
        logging.info(' Generating reports...')
        logging.info('-----------------------------------------------------------------------------------------')
        logging.info(' searching for jsons in /reports/raw...')
        jsons = [f for f in os.listdir(self.path) if os.path.isfile(os.path.join(self.path, f)) and f[-5:] == '.json']
        i = 0
        collection = {}
        for r in jsons:
            logging.info(' found: '+str(r))
            file_path = self.path + r
            with open(file_path, "r") as origin_data:
                logging.info(' raw report opened')
                original_dict = json.loads(origin_data.read())
                logging.info(' dictionary loaded')
                result_dict = self.format(original_dict)
                collection[i] = result_dict
                logging.info(' dictionary ' + str(i) + ' formatted')
            os.remove(file_path)
            i += 1
        # self.writedown_report(collection)
        self.htmlparse(collection)
        logging.info(' created html file.')
        logging.info(' all reports formatted and files created')

    def htmlparse(self,  dictionary):
        '''
            This function parses a json :param dictionary: and creates a html file.
        '''
        # feature = dictionary[0]["feature"]
        # feature = feature.replace(" ", "_")
        moment = timer.strftime("%d%m%Y", timer.gmtime()) + "-" + timer.strftime("%H%M")
        # new_file_path = "reports/" + str(moment) + "_REPORT_FEATURE_" + str(feature) + ".html"
        new_file_path = "reports/" + str(moment) + "_REPORT.html"
        with open(new_file_path, "w") as output:
            html = "<html><title>Feature Report</title><head><style>h1{font-family:\"Arial\";color:white;text-align:left} tbody{background-color:white;}"
            html += ".sidenav{height: 100%;width: 160px;position:fixed;z-index: 1;top:0;left:0;background-color: #111;overflow-x:hidden;padding-top:20px;}.sidenav a{padding: 6px 8px 6px 16px;text-decoration: none;font-size: 15px;color: #818181;display: block;}.sidenav a:hover {color: #f1f1f1;}.main {margin-left: 160px;padding: 0px 10px;}"
            html += "</style><script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script><script type=\"text/javascript\">google.charts.load('current', {'packages':['corechart']});google.charts.setOnLoadCallback(drawChart);"
            html += "function drawChart(){"
            i = 0
            for d in dictionary.values():
                html += self.graphic(d, str(i))
                i += 1
            html += "} $(window).resize(function(){drawChart()});</script>"
            html += "</head><body bgcolor=\"#367EFF\"><div class=\"sidenav\">"
            i = 0
            for d in dictionary.values():
                html += "<a href=\"#" + str(i) + "\">" + d["feature"] + "</a>"
                i += 1
            html += "</div><div class=\"main\">"
            i = 0
            for d in dictionary.values():
                feature = d["feature"]
                html += "<div >"
                        #style=\"height:100%\">"
                html += "<h1 id=\""+str(i)+"\"><color =\"white\"><b>" + str(feature) + "</b></color></h1>"
                html += "<table style=\"width:96%; margin-left: 2%; margin-right:2%\" align=\"center\" class=\"columns\">"
                html += "<tr><td><div id=\"piechart_div_" + str(i) + "\" style=\"border: 1px solid #ccc\"></div></td>"
                html += "<td><div id=\"barchart_div_" + str(i) + "\" style=\"border: 1px solid #ccc\"></div></td></tr></table>"
                html += "<div align=\"center\">"+json2html.convert(json=d)+"</div></div><hr>"# <br><br><br>
                i += 1
            # this ends div main
            html += "</div></body></html>"
            html = html.replace("<td>passed</td>", "<td bgcolor=\"green\" ><font color=\"white\">passed</font></td>")
            html = html.replace("<td>failed</td>", "<td bgcolor=\"red\" ><font color=\"white\">failed</font></td>")
            html = html.replace("<td>broken</td>", "<td bgcolor=\"black\" ><font color=\"white\">broken</font></td>")
            html = html.replace("<div align=\"center\"><table border=\"1\">", " <div align=\"center\"><table border=\"1\" style=\"width:96%; margin-left: 2%; margin-right:2%;\" >")
            html = html.replace("<th>steps</th><td><table border=\"1\">",
                                "<th>steps</th><td><table border=\"1\" style=\"width:100%\">")
            html = html.replace("<li><table border=\"1\">",
                                "<li><table border=\"1\" style=\"width:100%\">")
            output.write(html)

    def graphic(self, dictionary, i):
        '''
            This function creates the html script to generate two graphs using :param dictionary: data.
        '''
        passed, broken, failed = 0, 0, 0
        giventime, whentime, thentime = 0,0,0
        for s in dictionary["steps"]:
            if s["status"] == "failed":
                # logging.info(" failed ++")
                failed += 1
            elif s["status"] == "broken":
                # logging.info(" broken ++")
                broken += 1
            elif s["status"] == "passed":
                # logging.info(" passed ++")
                passed += 1
            if s["name"].find("Given") != -1:
                giventime = float(s["time"])
                # logging.info(" giventime: " + str(giventime))
            elif s["name"].find("When") != -1:
                whentime = float(s["time"])
                # logging.info(" whentime: " + str(whentime))
            elif s["name"].find("Then") != -1:
                thentime = float(s["time"])
                # logging.info(" thentime: " + str(thentime))

        code = "var data1_" + str(i) + " = new google.visualization.DataTable();data1_" + str(i) + ".addColumn('string', 'Results');data1_" + str(i) + ".addColumn('number', 'Quantity');"
        code += "data1_" + str(i) + ".addRows([['Passed'," + str(passed) + "],['Failed',"+str(failed)+"],['Broken',"+str(broken)+"]]);"
        code += "var data2_" + str(i) + " = new google.visualization.DataTable();data2_" + str(i) + ".addColumn('string', 'Method');data2_" + str(i) + ".addColumn('number', 'Time');"
        code += "data2_" + str(i) + ".addRows([['Given'," + str(giventime) +"],['When'," + str(whentime) + "],['Then'," + str(thentime) + "]]);"
        code += "var piechart_options_" + str(i) + " = {title:'Results',width:400,height:300, colors:['green','red','black'], is3D:true};"
        code += "var piechart_" + str(i) + " = new google.visualization.PieChart(document.getElementById('piechart_div_" + str(i) + "'));piechart_" + str(i) + ".draw(data1_" + str(i) + ", piechart_options_" + str(i) + ");"
        code += "var barchart_options_" + str(i) + " = {title:'Execution times',width:400,height:300,legend: 'none'};"
        code += "var barchart_" + str(i) + " = new google.visualization.BarChart(document.getElementById('barchart_div_" + str(i) + "'));barchart_" + str(i) + ".draw(data2_" + str(i) + ", barchart_options_" + str(i) + ");"

        return code

    def writedown_report(self, dictionary):
        '''
            This function writes down the formatted dictionary in a new JSON file.
        '''
        # file_path = "reports/"+dictionary["feature"] + " REPORT.json"
        file_path = "reports/collection.json"
        with open(file_path, "w") as output:
            output.write(json.dumps(dictionary, sort_keys=False, indent=4, separators=(',', ' : ')))
        return file_path

    def format(self, dictionary):
        '''
            This function formats a given :param dictionary: and :return:s a formatted one.
        '''
        result = {}
        name = dictionary["name"]; result["name"] = name
        feature = dictionary["labels"][2]["value"]; result["feature"] = feature
        status = dictionary["status"]; result["status"] = status
        global_start = dictionary["start"]
        global_stop = dictionary["stop"]
        global_time = global_stop - global_start; result["time"] = str(global_time / 1000)
        steps = []
        for s in dictionary["steps"]:
            step = {}
            step["name"] = s["name"]
            step["status"] = s["status"]
            start = s["start"]
            stop = s["stop"]
            step["time"] = str((stop - start) / 1000)
            if "statusDetails" in s:
                step["details"] = s["statusDetails"]
            steps.append(step)
        result["steps"] = steps
        return result
