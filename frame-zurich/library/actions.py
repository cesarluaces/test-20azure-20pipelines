import requests
import json
import library.security_control
from openpyxl import load_workbook
import os


def extract_data_from_excel(excel_file, sheet_name, data_name):
    """
    Function that opens an excel file and looks for a data label. When the function finds that data label, it takes the
    data from the next column in the same row.
    :param excel_file: The name of the excel from the function takes the data.
    :param sheet_name: The name of the sheet where the data ara stored.
    :param data_name: The name of the looked data.
    :return: The looked data.
    """
    wb = load_workbook(filename="../test-zurich/configs/excel_folder/" + excel_file)
    wr = ""
    ws = wb[sheet_name]
    counter = 1
    is_looking = True
    fail_search = True
    while is_looking and counter < 200:
        wc = ws["A" + str(counter)].value
        if data_name in wc:
            is_looking = False
            fail_search = False
            wr = ws["B" + str(counter)].value
        elif wc.__len__ == 0:
            is_looking = False
        else:
            counter += 1

    if fail_search:
        result = "NOT_FOUND"
        return result
    else:
        return wr


def extract_all_data_from_excel(excel_file, sheet_name):
    """
    Function that opens an excel file and extrack all the values stored. When the function finds all data, returns a
    dictionary with all teh values.
    :param excel_file: The name of the excel from the function takes the data.
    :param sheet_name: The name of the sheet where the data ara stored.
    :return: The dictionary with all the data.
    """
    dictionary_values = {}
    wb = load_workbook(filename="../test-zurich/configs/excel_folder/" + excel_file)
    wr = ""
    ws = wb[sheet_name]
    counter = 1
    is_looking = True
    fail_search = True
    while is_looking and counter < 200:
        wc = ws["A" + str(counter)].value
        if wc.__len__ != 0:
            fail_search = False
            wr = ws["B" + str(counter)].value
            dictionary_values[wc] = wr
            counter += 1
        else:
            is_looking = False

    if fail_search:
        result = "NOT_FOUND"
        return result
    else:
        return dictionary_values


def find_value_in_json(json_file, key):
    """
    Function that find the ID of a element include in a JSON file.
    :param json_file: The JSON file to find the ID
    :param key: The name of the element to find
    :return: The ID of the element
    """
    result = ""

    for element in json_file["value"]:
        if key in element["name"]:
            result = element["id"]

    return result


def make_token_header():
    """
    Function that make the header for a call. Include the token.
    :return:The Header for the call.
    """
    response = ""

    response = {
        "content-type": "application/json",
        "authorization": "Bearer " + library.security_control.get_token(),
        "Ocp-Apim-Subscription-Key": "2059ecc472c64bc697dfb0ab75be65d3"
    }

    return response


def check_Api_URL(call_type, url, querystring="", payload="", headers="", json_data=""):
    """
    Function that make a API call to an URL with the headers and data given in the parameters.
    :param call_type: The type of the call (GET, POST, PUT).
    :param url: The URL for the API call.
    :param querystring: The parameters for a GET call.
    :param payload: The data for a POST call.
    :param headers: The headers for the call.
    :param json_data: The JSON with the information to POST or PUT.
    :return: The response of the call.
    """
    response = ""
    if "GET" in call_type:
        response = requests.get(url, headers=headers, params=querystring, verify=False)
    elif "POST" in call_type:
        response = requests.post(url, data=payload, json=json_data, headers=headers, verify=False)
    elif "PUT" in call_type:
        response = requests.put(url, data=payload, json=json_data, headers=headers, verify=False)

    return response


def take_JSON_test_config(context, file):
    with open('../test-zurich/configs/config/' + file) as input_file:
        test_config = json.load(input_file)
    headers = {}
    url_base = ""
    url_comp = ""
    call_type = ""
    payload = ""
    querystring = ""
    json_data_from_file = ""
    json_data = ""
    Ocp_Apim_Subscription_Key_calculated = ""
    for element in test_config:
        if "call_type" in element:
            call_type = test_config[element]
        elif "payload" in element:
            payload = test_config[element]
        elif "url_base" in element:
            url_base = test_config[element]
        elif "url_comp" in element:
            url_comp = test_config[element]
        elif "json_to_post" in element:
            with open('./json_post_folder/' + test_config[element]) as json_file:
                json_data_from_file = json.load(json_file)
                json_data = open('./json_post_folder/' + test_config[element], 'rb').read()
        elif "headers" in element:
            for header in test_config[element]:
                version = str(header)
                if "content-type" in header:
                    headers_aux = {
                        "content-type": test_config[element][header]
                    }
                    headers.update(headers_aux)
                elif "Ocp-Apim-Subscription-Key" in header:

                    if test_config[element][header] is None:
                        Ocp_Apim_Subscription_Key_calculated = load_Ocp_Apim_Subscription_Key(context.environment)
                    else:
                        Ocp_Apim_Subscription_Key_calculated = test_config[element][header]
                    headers_aux = {
                        "Ocp-Apim-Subscription-Key": Ocp_Apim_Subscription_Key_calculated
                    }

                    headers.update(headers_aux)
                elif "cookie" in header:
                    headers_aux = {
                        "cookie": test_config[element][header]
                    }
                    headers.update(headers_aux)
                elif "authorization" in header:
                    if "Bearer " in test_config[element][header]:
                        headers_aux = {
                            "authorization": "Bearer " + library.security_control.get_token()
                        }
                    else:
                        headers_aux = {
                            "authorization": test_config[element][header]
                        }
                    headers.update(headers_aux)

    if url_base == "":
        url_base = load_url_base(context.environment)

    if Ocp_Apim_Subscription_Key_calculated == "":
        Ocp_Apim_Subscription_Key_calculated = load_Ocp_Apim_Subscription_Key(context.environment)
        headers_aux = {
            "Ocp-Apim-Subscription-Key": Ocp_Apim_Subscription_Key_calculated
        }
        headers.update(headers_aux)

    if context.request != "":
        json_data = context.request
    else:
        json_data = (json_data_from_file)
    print(json_data)

    for item in context.request_params:
        parameter = "{" + item["name"] + "}"
        value = item["value"]
        url_comp = url_comp.replace(parameter, value)

    context.url = (url_base + url_comp)
    context.header = headers

    response = check_Api_URL(call_type=call_type, url=url_base + url_comp, headers=headers, payload=payload,
                             querystring=querystring, json_data=json_data)

    return response


def load_environment():
    environment = ""
    file = '../test-zurich/configs/config/' + "config.system.execution.parameters.json"
    with open(file, 'r') as f:
        execution_vars = json.load(f)
    environment = execution_vars['current_environment']
    print("current environment: " + environment)
    return environment


def load_url_base(environment):
    url_base = ""
    file = '../test-zurich/configs/config/' + "config.system.environment.json"
    with open(file, 'r') as f:
        environment_vars = json.load(f)
    servers = environment_vars['servers']

    for server in servers:
        if server["env_name"] == environment:
            url_base = server["url_base"]

    print("base_url: " + url_base)

    return url_base


def load_Ocp_Apim_Subscription_Key(environment):
    Ocp_Apim_Subscription_Key = ""

    file = '../test-zurich/configs/config/' + "config.system.environment.json"
    with open(file, 'r') as f:
        environment_vars = json.load(f)
    servers = environment_vars['servers']

    for server in servers:
        if server["env_name"] == environment:
            Ocp_Apim_Subscription_Key = server["Ocp-Apim-Subscription-Key"]

    print("ocp_subcription_key: " + Ocp_Apim_Subscription_Key)

    return Ocp_Apim_Subscription_Key


def getServiceName(featureName):
    featureNameUnTokenized = (featureName.split("_"))
    return (featureNameUnTokenized[1])


def load_default_request(context):
    service = getServiceName(context.feature.name)
    context.service = service
    try:
        with open('../test-zurich/configs/config/' + service + '_requestFormat.json') as json_file:
            context.request = json.load(json_file)
    except:
        context.request = ""
