import logging
from sseclient import SSEClient
import _thread
import time


class TestThread:
    '''
        This class creates a thread to listen for other services responses.
    '''
    def __init__(self):
        '''
            Class constructor.
        '''
        logging.info('-----------------------------------------------------------------------------------------')
        logging.info(' Thread instance created.')
        self.url = "http://{{server}}/signalr/connect?transport=serverSentEvents&clientProtocol=1.5&connectionToken={{ConnectionToken}}&connectionData=%5B%7B%22name%22%3A%22notificationhub%22%7D%5D&tid=5"
        self.context = None
        self.event_list = ["EVENT_LIST"]
        self.end_event_thread = False
        logging.info('-----------------------------------------------------------------------------------------')

    def set_context(self, ctx):
        '''
            This function sets the local variable context with a given created context.
        '''
        logging.info('-----------------------------------------------------------------------------------------')
        logging.info(' Thread set_context start...')
        if ctx is not None:
            self.context = ctx
            logging.info(' Context loaded successfully.')
            logging.info(' Thread set_context ended OK.')
        else:
            logging.error(' Failed loading context: param "ctx" is None.')
            logging.error(' Thread set_context ended KO.')
            raise ValueError("Context is null")
        logging.info('-----------------------------------------------------------------------------------------')

    def refactor(self, text):
        '''
            This function refactors the given text with values from the context.
            It replaces keys between double {}, such as {{server}}.
            Returns the same text with substitutions made if possible.
        '''
        logging.info(' Thread refactor start...')
        try:
            if self.context is not None:
                if "{{" in text and "}}" in text:
                    key = text[text.find("{{") + 2: text.find("}}")]
                    logging.info(' Refactoring key ' + key)
                    value = self.context.search(key)
                    if value is not None:
                        return self.refactor(text[:text.find("{{")] + value + text[text.find("}}") + 2:])
                    else:
                        logging.error(' refactor: ' + key + ' not found.')
                        raise ValueError("Key not found in context")
                else:
                    logging.info(' Thread refactor ended.')
                    logging.info(
                        '-----------------------------------------------------------------------------------------')
                    return text
            else:
                logging.info(' Thread refactor ended.')
                logging.info(
                    '-----------------------------------------------------------------------------------------')
                return text
        except Exception as e:
            logging.exception(' - EXCEPTION: refactor()')
            logging.exception(' - ' + str(type(e)))
            logging.exception(' - ' + str(e.args))
            logging.exception(' Thread refactor() ended KO.')
            raise

    def launch(self, ctx):
        '''
            This function launches the async thread to listen SignalR.
        '''
        logging.info('-----------------------------------------------------------------------------------------')
        logging.info(' Thread launching')
        self.set_context(ctx)
        logging.info(' Context loaded.')
        self.url = self.refactor(str(self.url))
        _thread.start_new_thread(self.response_listener, ())
        logging.info(' Thread launched successfully')

    def response_listener(self):
        '''
            This is the function the child proccess will execute.
            It's role is to listen for messages in a URL and drop the message's data onto a local variable.
        '''
        while not self.end_event_thread:
            try:
                messages = SSEClient(self.url, timeout=(1, None))
                for msg in messages:
                    self.event_list.append(msg.data)
            except Exception as e:
                print("render and reload error", str(e))
            time.sleep(3)

    def wait_for_event(self, event):
        '''
            This function is used by the steps to wait for a _END_MACRO_ event.
        '''
        logging.info(' waiting for event: ' + str(event))
        end_this = False
        while not end_this:
            for item in self.event_list:
                if event in item:
                    end_this = True
            time.sleep(1)
        logging.info(' event happened.')

    def get_response(self):
        res = self.event_list
        self.event_list = []
        self.event_list = ["EVENT_LIST"]
        return res
