import os


def count_features():
    total_files = []
    with open('.\\cosa.txt', 'w') as file:
        for nombre_archivo in os.listdir("../test-zurich/features/bdd/"):
            total_files.append(nombre_archivo)

    return total_files


def edit_yml():
    number_of_files = count_features()
    with open('..\\azure-pipelines.yml', 'a') as file:
        for x in range(0, number_of_files.__len__()):
            file.write("\n" + "- task: PublishTestResults@2" + "\n")
            file.write("  inputs:" + "\n")
            file.write("    testResultsFormat: 'JUnit'" + "\n")
            file.write("    testResultsFiles: '**/" + number_of_files[x] + "'" + "\n")
            file.write("    testRunTitle: '" + number_of_files[x] + "'" + "\n")
            file.write("    buildPlatform: 'Python + Behave'" + "\n")
            file.write("    buildConfiguration: 'API Rest Testing -> pyTest execution'" + "\n")

    file.close()
