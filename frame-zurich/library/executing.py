

def make_behave_argv(verbose: bool=False, junit: bool=False, format_pretty: bool=False,
                     formatter: str=None, output: str=None, tags: [str, list]=None, conf_properties: str=None,
                     features_dir: str = None, **kwargs):
    params = ''
    if verbose:
        params = params + ' -v'
    if junit:
        params = params + ' --junit'
    if format_pretty:
        params = params + ' --format pretty'
    if formatter:
        params = params + ' -f ' + formatter
    if output:
        params = params + ' -o ' + output
    if tags:
        params = ''.join(' --tags=' + ','.join(tag if isinstance(tag, list) else [tag])
                         for tag in (tags if isinstance(tags, list) else [tags]))
    if conf_properties:
        params = params + ' -D Config_environment=' + conf_properties
    if kwargs:
        for k, v in kwargs.items():
            params = params + ' -D ' + k + '=' + v
    if features_dir:
        params = params + ' ' + features_dir
    return params
