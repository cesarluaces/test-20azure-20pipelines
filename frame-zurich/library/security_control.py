import json


def set_token(token):
    data = {'token': token}
    with open('./library/security.json', 'w') as input_file:
        json.dump(data, input_file)


def get_token():
    token = ""
    with open('./library/security.json') as input_file:
        security_config = json.load(input_file)
        token = security_config["token"]

    return token
